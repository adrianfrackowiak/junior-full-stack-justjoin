## JustJoin - Junior Full-Stack Developer

Installation and usage:

Backend (http://localhost/3001):

```
yarn install
yarn start
```

Frontend (http://localhost/3000):

```
yarn install
yarn run dev
```

Stack:

- Next.js
- ChakraUI
- TypeScript
- React Map GL

- NestJS
- TypeScript
