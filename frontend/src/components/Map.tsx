import React, { useEffect, useState } from 'react';
import ReactMapGL, { Marker } from 'react-map-gl';
import { IoMdPin } from 'react-icons/io';
import { Offer } from '../interfaces/offers.interface';

interface Viewport {
  width: string;
  height: string;
  latitude: number;
  longitude: number;
  zoom: number;
}

interface Props {
  offers?: Offer[];
}

const Map: React.FC<Props> = ({ offers }) => {
  const [viewport, setViewport] = useState<Viewport>({
    width: '100%',
    height: '100%',
    latitude: 52,
    longitude: 19.4,
    zoom: 6,
  });

  return (
    <ReactMapGL
      {...viewport}
      mapStyle="mapbox://styles/mapbox/streets-v9"
      mapboxApiAccessToken="pk.eyJ1IjoiYWRyaWFuZnJhY2tvd2lhayIsImEiOiJja3U1ZTEyMTYybmJ3MnVxaHVsMG15bnYyIn0.g5XYIT2pOSlCSDt_kjQe1w"
      onViewportChange={(viewport: Viewport) => setViewport(viewport)}
    >
      {offers
        ? offers.map((offer, key) => (
            <Marker key={key} longitude={offer.lon} latitude={offer.lat}>
              <IoMdPin size={30} fill={'#1fb179'} />
            </Marker>
          ))
        : ''}
    </ReactMapGL>
  );
};

export default Map;
