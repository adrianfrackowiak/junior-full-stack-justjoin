import { Box, Flex, Text } from '@chakra-ui/react';
import React, { SetStateAction, useEffect, useState } from 'react';
import { Offer } from '../../interfaces/offers.interface';

interface Props {
  filterOffers: string;
  offers?: Offer[];
  setOffers: Function;
}

export const OffersList: React.FC<Props> = ({
  filterOffers,
  offers,
  setOffers,
}) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    fetch(`http://localhost:3001/offers?title=${filterOffers}`)
      .then(res => {
        return res.json();
      })
      .then(data => {
        setOffers(data);
        setIsLoading(false);
      });
  }, [filterOffers, setOffers]);

  if (isLoading) return <h1>Loading...</h1>;

  return (
    <Flex
      backgroundColor="#19a56f"
      color="white"
      height="100%"
      overflow="scroll"
    >
      <Box display="flex" flexDirection="column" w="100%" padding={12}>
        {offers?.length === 0 ? (
          <Text>Sorry, there are no job offers for: {filterOffers}</Text>
        ) : (
          offers?.map((offer, key) => {
            const date = new Date(offer.publishedAt);
            return (
              <Box
                padding={2}
                display="flex"
                flexDirection="column"
                marginBottom={4}
                boxShadow="xl"
                rounded="md"
                backgroundColor="#1fb179"
                key={key}
              >
                <Text fontSize={20} margin={0} fontWeight="bold">
                  {offer.title}
                </Text>
                <Text fontSize={12}>{offer.id}</Text>

                <Text fontSize={12}>
                  {date.getDate()}.{date.getMonth() + 1}.{date.getFullYear()}
                </Text>
              </Box>
            );
          })
        )}
      </Box>
    </Flex>
  );
};
