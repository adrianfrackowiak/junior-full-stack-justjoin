import { Box, Flex } from '@chakra-ui/react';
import React, { useState } from 'react';
import dynamic from 'next/dynamic';
import { Header } from '../header/Header';
import { OffersList } from '../offers/OffersList';
import { Offer } from '../../interfaces/offers.interface';
const Map = dynamic(() => import('../map/Map'), { ssr: false });

const Home = () => {
  const [filterOffers, setFilterOffers] = useState<string>('');
  const [offers, setOffers] = useState<Offer[]>();

  const onFilterOffers = (text: string) => {
    setFilterOffers(text);
  };

  const onSetOffers = (data: Offer[]) => {
    setOffers(data);
  };

  return (
    <Flex height="100%" flexDirection="column">
      <Header onFilterOffers={onFilterOffers} />
      <Flex flexDirection="row" flex={1} minHeight={0}>
        <Box flexBasis={600}>
          <OffersList
            filterOffers={filterOffers}
            setOffers={onSetOffers}
            offers={offers}
          />
        </Box>
        <Box flex={1}>
          <Map offers={offers} />
        </Box>
      </Flex>
    </Flex>
  );
};

export default Home;
