import { Button, Flex, Input } from '@chakra-ui/react';
import React, { useState } from 'react';

interface Props {
  onFilterOffers: Function;
}

export const Header: React.FC<Props> = props => {
  const [filterOffers, setFilterOffers] = useState<string>();

  return (
    <Flex
      height={100}
      alignItems="center"
      justifyContent="center"
      backgroundColor="#dedede"
    >
      <Flex fontSize={24} width="30%">
        <Input
          placeholder="React Developer..."
          size="lg"
          variant="filled"
          boxShadow="xl"
          rounded="md"
          focusBorderColor="#1fb179"
          value={filterOffers}
          onChange={e => setFilterOffers(e.target.value)}
        />
        <Button
          colorScheme="green"
          variant="solid"
          size="lg"
          marginLeft={4}
          onClick={() => props.onFilterOffers(filterOffers)}
        >
          Search
        </Button>
      </Flex>
    </Flex>
  );
};
