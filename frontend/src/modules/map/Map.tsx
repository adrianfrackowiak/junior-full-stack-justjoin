import React from 'react';
import { Box, Flex } from '@chakra-ui/react';
import dynamic from 'next/dynamic';
import { Offer } from '../../interfaces/offers.interface';

const Map = dynamic(() => import('../../components/Map'), {
  ssr: false,
});

interface Props {
  offers?: Offer[];
}

const MapModule: React.FC<Props> = ({ offers }) => {
  return (
    <Flex
      alignItems="center"
      justifyContent="center"
      backgroundColor="#e8d8ff"
      height="100%"
      id="mapid"
      width="100%"
    >
      <Map offers={offers}></Map>
    </Flex>
  );
};

export default MapModule;
