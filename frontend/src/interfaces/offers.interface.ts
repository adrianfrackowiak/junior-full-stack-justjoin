export interface Offer {
  id: string;
  title: string;
  publishedAt: string;
  lon: number;
  lat: number;
}
