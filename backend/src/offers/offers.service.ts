import { Injectable } from "@nestjs/common";
import { data } from "data";
import { Offer } from "./offers.interface";

@Injectable()
export class OffersService {
  private readonly offers: Offer[] = data.map((offer) => {
    return {
      id: offer.id,
      title: offer.title,
      publishedAt: offer.published_at,
      lon: offer.longitude,
      lat: offer.latitude,
    };
  });

  getOffers(title?: string): Offer[] {
    if (title) {
      return this.offers.filter((offer) =>
        offer.title.toLowerCase().includes(title.toLowerCase())
      );
    }

    return this.offers;
  }
}
