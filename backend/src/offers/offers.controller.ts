import { Controller, Get, Query } from "@nestjs/common";
import { OffersService } from "src/offers/offers.service";
import { Offer } from "./offers.interface";

@Controller("offers")
export class OffersController {
  constructor(private readonly offersService: OffersService) {}

  @Get()
  getOffers(@Query("title") title?: string): Offer[] {
    return this.offersService.getOffers(title);
  }
}
